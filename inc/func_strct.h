#include <sys/shm.h>
#define SHM_SIZE sizeof(address)

typedef struct address { 
    int addr;
    char type; 
} address;

typedef struct Node{
    unsigned int page;
    int isWritten;
} Node;


Node addressToNode(address);
void PM(const char*,int,int,int,address*);
void MM(address*,address*,int,int,int,int,int);

inline void deleteSharedMemory(address *sh_mem,int shm_id) {
    shmdt(sh_mem);
    shmctl(shm_id, IPC_RMID, 0);
}
