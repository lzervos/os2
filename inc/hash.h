#include <vector>

struct Node;

class Hash 
{ 
    int BUCKET;    // No. of buckets 
  
    // Pointer to an array containing buckets 
    std::vector<Node> *table; 
public: 
    Hash(int V);  // Constructor 
    
    ~Hash();    // Destructor

    // Search a page in HashTable
    int Search(address);

    //Cleaning HashTable
    int Flush();

    void updateBit(int,int);
    // inserts a key into hash table 
    void insertItem(Node); 
  
    // deletes a key from hash table 
    void deleteItem(Node); 
  
    // hash function to map values to key 
    int hashFunction(int);
  
    void displayHash(); 
}; 