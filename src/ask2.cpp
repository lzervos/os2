#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <unistd.h>
#include "../inc/func_strct.h"
#include "../inc/semph.h"
#include "../inc/hash.h"



int main(int argc,char **argv) {
    int max_pf,total_num,q,max_ref;
    int wpid,stat_val;
    int shm_id,shm_id1;
    pid_t pid[2];
    address *add,*add1;

    // int SHM_SIZE=q*sizeof(address);

    if(argc==4) {
        max_pf=atoi(argv[1]);
        total_num=atoi(argv[2]);
        q=atoi(argv[3]);
        max_ref=q;
    }
    else if(argc==5) {
        max_pf=atoi(argv[1]);
        total_num=atoi(argv[2]);
        q=atoi(argv[3]);
        max_ref=atoi(argv[4]);  
    }
    else {
        fprintf(stderr,"Wrong arguments\n");
        exit(1);
    }

    int semid=sem_init();
    if((shm_id=shmget(IPC_PRIVATE,SHM_SIZE,IPC_CREAT|0666)) == -1) {
		perror("Shmget failed");
		exit(EXIT_FAILURE);	
	}

    if((shm_id1=shmget(IPC_PRIVATE,SHM_SIZE,IPC_CREAT|0666)) == -1) {
		perror("Shmget failed");
		exit(EXIT_FAILURE);	
	}

    if((add=(address*)shmat(shm_id,NULL,0)) == (address*) -1) {
		perror("Shmat failed");
		exit(EXIT_FAILURE);
	}

    if((add1=(address*)shmat(shm_id1,NULL,0)) == (address*) -1) {
		perror("Shmat failed");
		exit(EXIT_FAILURE);
	}

    pid[0]=fork();
    if(pid[0] == 0) {
        PM("../workloads/bzip.trace",q,max_ref,semid,add);
    }

    pid[1]=fork();
    if(pid[1] == 0) {
        PM("../workloads/gcc.trace",q,max_ref,semid,add1);
    }
    
    MM(add,add1,max_pf,q,max_ref,total_num,semid);

    while((wpid=wait(&stat_val))>0) {
        std::cout<<"Process ends with pid "<<wpid<<std::endl;
    }

    deleteSharedMemory(add,shm_id);
    deleteSharedMemory(add1,shm_id1);
}   
