#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "../inc/func_strct.h"
#include "../inc/hash.h"
#include "../inc/semph.h"

Node addressToNode(address ad) {
    Node n;
    n.page=ad.addr >> 12;
    if(ad.type=='W') {
        n.isWritten=1;
    }
    else {
        n.isWritten=0;
    }
    return n;
}

void PM(const char* filename,int q,int max_ref,int semid,address *ad) {
    FILE *file;
    address adr;

    // wat(semid,EMPTY);
    file=fopen(filename,"r");
    for(int j=0; j<max_ref/(q*2); j++) {
        for(int i=0; i<q; i++) {
            if(strcmp(filename,"../workloads/gcc.trace")==0) {
                wat(semid,EMPTY1);
            }
            else {
                wat(semid,EMPTY);
            }

            fscanf(file,"%d",&(adr.addr));
            fscanf(file," ");
            fscanf(file,"%c",&(adr.type));
            fscanf(file," ");

            memcpy(ad,&adr,SHM_SIZE);
            signl(semid,EMPTY2);
        }
    }
    std::cout<<"End of process"<<std::endl;
    fclose(file);
    exit(0);
}

void MM(address *add,address *add1,int max_pf,int q,int max_ref,int total_num,int semid) {
    Hash *h1=new Hash(total_num/2);
    Hash *h2=new Hash(total_num/2);
    address ad;
    Node n;
    int i,j,temp;
    int count = 0; 
	int DiskWrites = 0,DiskReads = 0,MemWrites = 0, MemReads = 0; 
	int total_pf=0,pf1 = 0, pf2 = 0; 

    
    for(int j=0; j<max_ref/(q*2)+1; j++) {
        std::cout<<"Reading from Process1"<<std::endl;
        for(i=0; i<q; i++) {
            signl(semid,EMPTY);
            wat(semid,EMPTY2);
            memcpy(&ad,add,SHM_SIZE);
            count++;
            
        //Search HashTable and update
            if ((temp=h1->Search(ad))<0) {
                pf1++;
                total_pf++;
                if(pf1 > max_pf) {
                    DiskWrites+=h1->Flush();
                    pf1=1;
                }
                Node n=addressToNode(ad);
                h1->insertItem(n);
                DiskReads++;

            }
            else {
                if(ad.type=='W') {
                    h1->updateBit(ad.addr,temp);
                    MemWrites++;
                }
                else {
                    MemReads++;
                }
            }
        
        } 
        std::cout<<"Reading from Process2"<<std::endl;
        for(i=0; i<q; i++) {
            signl(semid,EMPTY1);
            wat(semid,EMPTY2);
            memcpy(&ad,add1,SHM_SIZE);
            count++;
        

        //Search HashTable and update
            if ((temp=h2->Search(ad))<0) {
                pf2++;
                total_pf++;
                if(pf2 > max_pf) {
                    DiskWrites+=h2->Flush();
                    pf2=1;
                }
                Node n=addressToNode(ad);
                h2->insertItem(n);
                DiskReads++;

            }
            else {
                if(ad.type=='W') {
                    h2->updateBit(ad.addr,temp);
                    MemWrites++;
                }
                else {
                    MemReads++;
                }
            }

        }
        std::cout<<"DiskReads:"<<DiskReads<<std::endl;
        std::cout<<"DiskWrites:"<<DiskWrites<<std::endl;
        std::cout<<"MemReads:"<<MemReads<<std::endl;
        std::cout<<"MemWrites:"<<MemReads<<std::endl;
        delete h1;
        delete h2;
    }
}
