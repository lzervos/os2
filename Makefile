CC = g++

EXE = ask2

SRC_DIR = src
OBJ_DIR = obj
INC_DIR = inc

SRC = $(wildcard $(SRC_DIR)/*.cpp)
OBJ = $(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)
DEPS = $(wildcard $(INC_DIR)/*.h*)

CCFLAGS = -std=c++11 -O3

.PHONY: clean
.PHONY: directories

$(EXE): directories $(OBJ)
	$(CC) $(CCFLAGS) -o $@ $(OBJ)
	#
	#	OK! now try the program by typing:
	#		./ask2 max_page_faults total_frames q max_references

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(DEPS)
	$(CC) $(CCFLAGS) -c $< -o $@

clean:
	rm -f $(EXE) $(OBJ)

directories:
	mkdir -p obj

#compile for gdb
d: CCFLAGS += -g3

d: $(EXE)
